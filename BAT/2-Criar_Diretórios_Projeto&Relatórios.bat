@echo off
@break off
@title Criar novo Diret�rio, caso n�o exista - MDAguiar
@color 0a
@cls
@CHCP 1252 >NUL

setlocal EnableDelayedExpansion

if not exist "C:\Source - Automa��o - AC" (
  mkdir "C:\Source - Automa��o - AC"
  if "!errorlevel!" EQU "0" (
    echo Diret�rio Source - Automa��o - AC criado com Sucesso!
  ) else (
    echo Erro ao criar Diret�rio C:\Source - Automa��o - AC
  )
) else (
  echo Source - Automa��o - AC Diret�rio j� Existe!
)

if not exist "C:\Relat�rios\Screenshots" (
  mkdir "C:\Relat�rios\Screenshots"
  if "!errorlevel!" EQU "0" (
    echo Diret�rio Relat�rios\Screenshots criado com Sucesso!
  ) else (
    echo Erro ao criar Diret�rio Relat�rios\Screenshot
  )
) else (
  echo  Relat�rios\Screenshots Diret�rio j� Existe!
)
pause



echo off
cls