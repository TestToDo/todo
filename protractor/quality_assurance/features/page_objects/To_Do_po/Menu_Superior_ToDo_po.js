'use strict';
const Helper = require('../../shared_libs/helper');

class MenuSuperioToDoPage {
	constructor() {
        this.helper = new Helper();
        //Menu Superior Esquerdo
        //this.mnHome = element(by.linkText('Home'))
        this.mnHome = element(by.css('ul>li:nth-child(1)'))
        
        //this.mnMyTasks = element(by.linkText('My Tasks'))
        this.mnMyTasks = element(by.css('ul>li:nth-child(2)'))

        //Menu Superior Direito
        //this.linkSignIn  = element(by.linkText('Sign In'))
        this.mnSignIn = element(by.css('.nav.navbar-nav.navbar-right>li:nth-child(1)'))
        this.mnSignInH =(by.css('.nav.navbar-nav.navbar-right>li:nth-child(1)'))

         //this.linkRegister  = element(by.linkText('Register'))
         this.mnRegister = element(by.css('.nav.navbar-nav.navbar-right>li:nth-child(2)'))

         //LOGADO
         this.msgBemVindo = $('.nav.navbar-nav.navbar-right>li:nth-child(1)')
        
	}

	clicarMenuHome() {
        this.helper.elementIsClickable(this.mnHome)
        return this.mnHome.click();
	}

	clicarMenuMyTasks() {
		this.helper.elementIsClickable(this.mnMyTasks);
		return this.mnMyTasks.click();
    }
    
    clicarMenuSignIn() {
		this.helper.elementIsClickable(this.mnSignIn);
		return this.mnSignIn.click();
    }
    descatarSignIn() {
      this.helper.highlightElement(this.mnSignInH)
    }

    clicarMenuRegister() {
		this.helper.elementIsClickable(this.mnRegister);
		return this.mnRegister.click();
    }

}

module.exports = MenuSuperioToDoPage
