'use strict';
const Helper = require('../../shared_libs/helper');

class SignUpToDoPage {
	constructor() {
        this.helper = new Helper();
        this.titlePage = $('div.panel-heading')
        this.cpName = element(by.id('user_name'))
        this.cpEmail = $('#user_email')
        this.cpPass = $('#user_password')
        this.cpPassConf = $('#user_password_confirmation')
        this.btSignUp = element(by.name('commit'));
        this.lkSignIn = element(by.linkText('Sign In'))
	}

	preencherCpNome(text) {
		this.helper.elementIsClickable(this.cpName);
		return this.cpName.sendKeys(text);
    }
    
    preencherCpEmail(text) {
		this.helper.elementIsClickable(this.cpEmail);
		return this.cpEmail.sendKeys(text);
	}

	preencherCpSenha(text) {
		this.helper.elementIsClickable(this.cpPass);
		return this.cpPass.sendKeys(text);
    }

    preencherCpConSenha(text) {
		this.helper.elementIsClickable(this.cpPassConf);
		return this.cpPassConf.sendKeys(text);
    }
    
	clicarBtSignUp() {
		this.helper.elementIsClickable(this.btSignUp);
		return this.btSignUp.click();
    }
    
    clicarLkSignIn() {
		this.helper.elementIsClickable(this.lkSignIn);
		return this.lkSignIn.click();
	}

}

module.exports = SignUpToDoPage;
