'use strict'
const Helper = require('../../shared_libs/helper')


class myTasksPage {
  constructor () {
    this.helper = new Helper()
    this.titlePage = $('h1')
    this.titleGrid = $('.panel-heading') 
    this.titleGridH =(by.css('div.panel-heading')) 
    this.cpDescTask  = element(by.model('newTask.body'))
    this.btAdic = element(by.css('[ng-click="addTask()"]'))
    //VER
    this.btRemove = element(by.buttonText('Remove'))
    this.cbDone = element(by.model('task.completed'))
    this.cbPublic = element(by.model('task.public'))
    this.btManSubTask = $('button[ng-click="editModal(task)"]')
    this.clDone = $('.table>thead>tr>th:nth-child(1)')
    this.clTodo = $('.table>thead>tr>th:nth-child(2)')
    this.clPublic = $('.table>thead>tr>th:nth-child(3)')
    this.clPublic = $('.table>thead>tr>th:nth-child(4)')
    this.clRemove = $('.table>thead>tr>th:nth-child(5)')
    //this.itTable = $('tr:nth-child(<numero_linha>) > td:nth-child(<numero_coluna>)')

}

descatarTituloGrid () {
  this.helper.highlightElement(this.titleGridH)
}

preencherCpTask(text) {
    this.helper.elementIsClickable(this.cpDescTask)
    return this.cpDescTask.sendKeys(text)
  }

  clicarBtAdic () {
    this.helper.elementIsClickable(this.btAdic)
    return this.btAdic.click()
  }

  clicarBtRemove () {
    this.helper.elementIsClickable(this.btRemove)
    return this.btRemove.click()
  }

  clicarCbDone () {
    this.helper.elementIsClickable(this.cbDone)
    return this.cbDone.click()
  }

  clicarCbPublic () {
    this.helper.elementIsClickable(this.cbPublic)
    return this.cbPublic.click()
  }

  clicarBtManSubTask () {
    this.helper.elementIsClickable(this.btManSubTask)
    return this.btManSubTask.click()
  }


}

module.exports = myTasksPage