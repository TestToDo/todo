'use strict';
const Helper = require('../../shared_libs/helper');

class HomePage {
	constructor() {
		this.helper = new Helper();
		this.menSucLoginH = (by.css('div.alert.alert-info'));
		this.menSucLogin = element(by.css('div.alert.alert-info'));
		//this.btSignUp = element(by.linkText('Sign Up'));
		this.btSignUp = element(by.css('a.btn.btn-lg.btn-success'))
		this.btTestDesc = element(by.linkText('Test Description'));
		//this.TestDesc = element(by.className('.btn.btn-lg.btn-danger'))
		//LOGADO
		//this.btMyTasks = element(by.buttonText('My Tasks'));
		this.btMyTasks = $('a.btn.btn-lg.btn-success');
		this.btMyTasksH = (by.css('a.btn.btn-lg.btn-success'));
	}

	descatarMenSucLogin() {
		this.helper.highlightElement(this.menSucLoginH);
	}

	clicarBtSignUp() {
		this.helper.elementIsClickable(this.btSignUp);
		return this.btSignUp.click();
	}

	clicarBtTestDesc() {
		this.helper.elementIsClickable(this.btTestDesc);
		return this.btTestDesc.click();
	}
	descatarBtMyTasks () {
		this.helper.highlightElement(this.btMyTasksH)
	  }

	clicarBtMyTasks() {
		this.helper.elementIsClickable(this.btMyTasks);
		return this.btMyTasks.click();
	}
}

module.exports = HomePage;
