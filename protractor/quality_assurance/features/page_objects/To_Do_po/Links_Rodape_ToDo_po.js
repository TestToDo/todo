'use strict';
const Helper = require('../../shared_libs/helper');

class LinksRodapePage {
	constructor() {
		this.helper = new Helper();
        this.lkAvCode = $('p > a:nth-child(1)')
        //this.lkAvCode = elemen(by.linkText('Avenue Code'))
        //this.lkAvCode = element(by.css('a[href*="https://www.avenuecode.com.br/"]'))

        this.lkTestDesc = $('p > a:nth-child(2)')
        //this.lkTestDesc = elemen(by.linkText('Test Description - User Stories'))
        //this.lkTestDesc = element(by.css('a[href*="user-stories"]'))

        this.lkBug = $('p > a:nth-child(3)')
        //this.lkTestDesc = elemen(by.linkText('Track a Bug'))
        //this.lkTestDesc = element(by.css('a[href*="/bugs"]'))

	}

	clicarLinkAvCode() {
		this.helper.elementIsClickable(this.lkAvCode);
		return this.lkAvCode.click();
    }
    
	clicarLinkTestDesc() {
		this.helper.elementIsClickable(this.lkTestDesc);
		return this.lkTestDesc.click();
    }
    clicarLinkBugs() {
		this.helper.elementIsClickable(this.lkBug);
		return this.lkBug.click();
    }
}

module.exports =  LinksRodapePage
