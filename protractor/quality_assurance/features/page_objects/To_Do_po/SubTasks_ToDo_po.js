'use strict'
const Helper = require('../../shared_libs/helper')


class subTasksPage {
  constructor () {
    this.helper = new Helper()
    //this.titleModalSub = $('h3:contains("Editing Task")')
   // this.titleModalSub = element(by.css('h3.modal-title.ng-binding'))
   this.titleModalSub = element(by.className('modal-title ng-binding'))
    this.cpToDo = element(by.model('task.body'))
    this.cpSubTaskDesc = element(by.model('subTask.body'))
    this.cpDueDate = element(by.model('subTask.due_date'))
    this.btAddSubTask = element(by.id('add-subtask'))
    this.btCloseSubTaskModal = $('button[ng-click="close()"]')
    this.titleSubTaskGrid = $('h4:contains("SubTasks of this ToDo:")')
    this.clDone = $('div:nth-child(4)>.table>thead>tr>th:nth-child(1)')
    this.clSubTask =  $('div:nth-child(4)>.table>thead>tr>th:nth-child(2)')
    this.clRemSubTask =  $('div:nth-child(4)>.table>thead>tr>th:nth-child(3)')
    //this.itTableSub = $('tr:nth-child(<numero_linha>) > td:nth-child(<numero_coluna>)')

    this.btRemSubTask =  $('button[ng-click="removeSubTask(sub_task)"]')
    //this.btRemSubTask =  $('button[ng-click="removeSubTask(sub_task)"]').get(<0 para remover primeira subtask da tabela e assim sucessivamente>)
}

preencherCpToDo(text) {
    this.helper.elementIsClickable(this.cpToDo)
    return this.cpToDo.sendKeys(text)
  }


  preencherCpSubTaskDesc(text) {
    this.helper.elementIsClickable(this.cpSubTaskDesc)
    return this.cpSubTaskDesc.sendKeys(text)
  }

  preencherCpDueDate(text) {
    this.helper.elementIsClickable(this.cpDueDate)
    return this.cpDueDate.sendKeys(text)
  }

  clicarBtAddSubTask () {
    this.helper.elementIsClickable(this.btAddSubTask)
    return this.btAddSubTask.click()
  }

  clicarBtRemSubTask () {
    this.helper.elementIsClickable(this.btRemSubTask)
    return this.btRemSubTask.click()
  }
  clicarBtClose () {
    this.helper.elementIsClickable(this.btCloseSubTaskModal)
    return this.btCloseSubTaskModal.click()
  }


}

module.exports = subTasksPage