'use strict'
const Helper = require('../../shared_libs/helper')


class SignInPage {
  constructor () {
    this.helper = new Helper()
    this.titlePage = $('h4')
    this.email = element(by.id('user_email'))
    this.emailH = (by.id('user_email'))
    this.senha = element(by.id('user_password'))
    this.senhaH = (by.id('user_password'))
    this.cbRememberMe = (by.id('user_remember_me'))
    this.btSignIn = element(by.name('commit'))
    this.btSignInH = (by.name('commit'))
    this.lkSignUp = element(by.linkText('Sign up'))

}


preencherEmail (text) {
    this.helper.elementIsClickable(this.email)
    return this.email.sendKeys(text)
  }

  preencherSenha (text) {
    this.helper.elementIsClickable(this.senha)
    return this.senha.sendKeys(text)
  }

  clicarBtSignIn () {
    this.helper.elementIsClickable(this.btSignIn)
    return this.btSignIn.click()
  }

  descatarCpEmail () {
    this.helper.highlightElement(this.emailH)
  }
  descatarCpSenha () {
    this.helper.highlightElement(this.senhaH)
  }
  descatarBtSignIn () {
    this.helper.highlightElement(this.btSignInH)
  }

  clicarCbRemenberMe () {
    this.helper.elementIsClickable(this.cbRemenberMe)
    return this.cbRemenberMe.click()
  }

  clicarLkSignUp() {
		this.helper.elementIsClickable(this.lkSignUp);
		return this.lkSignUp.click();
	}

  
}

module.exports = SignInPage