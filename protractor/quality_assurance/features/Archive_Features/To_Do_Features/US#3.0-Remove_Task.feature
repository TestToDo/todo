#language: pt
@avenue
#@1.3.1
#@1.3.2
#@1.4
#@1.5



Funcionalidade: Exclusão de Tasks

Cenário: 3.0-Excluir Tasks

  Dado que possuo ao menos um um registro de task cadastrado 
  
  Quando clico no botão Remove
  
  Então o sistema remove o registro de tarefa
