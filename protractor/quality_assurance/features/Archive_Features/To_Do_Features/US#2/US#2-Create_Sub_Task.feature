#language: pt
@avenue

Funcionalidade: Criar Sub Tarefa
Como usuário do aplicativo ToDo
Eu deveria ser capaz de criar uma subtarefa
Para que eu possa dividir minhas tarefas em pedaços menores

Contexto: Passos para Satisfazer Pré-condições e validar  a funcionalidade Subtask
 Quando efetuo login
 E clico no menu My Tasks
 E crio uma task simples

 
@2.1
Esquema do Cenário: 2.1-Validar existencia do botão Manager Subtasks para os registros de task cadastrados
O usuário deve ver um botão rotulado como Manage Subtasks

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando preencho os campos "<SubTaskDescription>" e "<DueDate>" e clico no botão Add
  Então um novo registro de SubTask será incluído na grid SubTasks of this ToDo.

  Exemplos: 
  |SubTaskDescription                                 |DueDate     |
  |ironmaximo2@hotmail.com                    |10/10/2019|

@2.2
Esquema do Cenário: 2.2-Validar que o botão Manager Subtasks apresenta a informação do número de subtasks criadas para o registro de task em questão
 Este botão deve ter o número de subtarefas criadas para essas tarefas

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  E preencho os campos "<SubTask Description>" e "<DueData>" e clico no botão Add
  E clico em close
  Então o botão Manage Subtasks apresenta o número de subtasks da tarefa

  Exemplos: 
  |SubTask Description                                 |DueData     |
  |Subtask2                                                   |10/10/2019|

@2.3.1
Cenário: 2.3.1-Validar que os campos  Id e Descrição da Tarefa são somente leitura na tela modal de SubTasks
 Esse pop-up deve ter um campo somente leitura com o ID e a descrição da tarefa

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então valido que as informações de id e descrição da tarefa são somente leitura.

@2.3.2.1
Esquema do Cenário: 2.3.2.1-Validar que os campos  Sub Task Description e Due Date são exibidos no modal e aceitam as entradas especificadas
 Deve haver um formulário para que você possa inserir a Descrição (250
 caracteres) e data de vencimento da Sub Tarefa  (formato MM / dd / aaaa)

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando preencho os campos "<SubTask Description>" e "<DueData>" e clico no botão Add
  Então um novo registro de SubTask será incluído na grid SubTasks of this ToDo.

  Exemplos: 
  |SubTaskDescription                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |DueData     |
  |0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789 |10/10/2019|
@2.3.2.2
Esquema do Cenário: 2.3.2.2-Validar que os campos  Sub Task Description e Due Date são exibidos no modal e não aceitam as entradas diferentes das especificadas
 Deve haver um formulário para que você possa inserir a Descrição (250
 caracteres) e data de vencimento da Sub Tarefa  (formato MM / dd / aaaa)

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando preencho os campos "<SubTask Description>" e "<DueData>" e clico no botão Add  
  Então o sistema não inclui a nova subtarefa

  Exemplos: 
  |SubTaskDescription                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |DueDate|
  |01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891|30/10/19|

@2.3.3
Esquema do Cenário: 2.3.3-Validar que é necessário clicar no botão Add para inserir uma nova Sub Tarefa
 O usuário deve clicar no botão Adicionar para adicionar uma nova subtarefa

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando preencho os campos "<SubTaskDescription>" e "<DueDate>" e pressiono a tecla Enter.
  Então o sistema não inclui a nova subtarefa
 Exemplos: 
  |SubTaskDescription                                 |DueDate    |
  |Subtask2                                                  |10/10/2019|

@2.3.4
Cenário: 2.3.4-Validar que os campos  Sub Task Description e Due Date são de preenchimento obrigatório
  Descrição da tarefa e a data de vencimento são campos obrigatórios

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando sem preencher os campos SubTask Description e DueDate  clico em Add
  Então o sistema não inclui a nova subtarefa

@2.3.5
Esquema do Cenário: 2.3.5-Validar que  os novos registros de Sub Tarefas são anexados a uma grid ao final da tela modal
  As subtarefas adicionadas devem ser anexadas na parte inferior da modal

  Dado que estou na tela My Tasks com ao menos um registro de task cadastrado
  Quando clico em Manage Subtasks.
  Então o sistema apresenta a modal Editing Task <número da task>
  Quando preencho os campos "<SubTask Description>" e "<DueData>" e clico no botão Add  
  Então um novo registro de SubTask será incluído na grid SubTasks of this ToDo.

 Exemplos: 
  |SubTask Description                                 |DueData     |
  |Subtask3                                                   |11/12/2019|

@incluiSubtask
Cenário: Inclusão rápida de subTasks

 Quando incluo um registro de SubTask




  









