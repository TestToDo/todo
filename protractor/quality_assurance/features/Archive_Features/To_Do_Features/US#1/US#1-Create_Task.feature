#language: pt
@avenue
@US#1

Funcionalidade: Criar Tarefa
Como usuário do aplicativo ToDo
Eu gostaria de ser capaz de criar uma tarefa
Para que então, eu possa gerenciar minhas tarefas

@1.1
Esquema do Cenário: 01-Validar apresenção do link My Tasks na NavBar em todas as telas do sistema ToDo
-O usuário sempre deve ver o link My Tasks na NavBar
-Clicar neste link redirecionará o usuário para uma página com todas as tarefas criadas até o momento

 Dado que estou na tela Home do sistema ToDo, area não logada
 Então verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no botão Sign Up da tela Home
 Então o sistema navega para a tela Sign Up
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Home
 E clico no botão Test Description
 Então o sistema navega para a tela User Stories
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Home
 E clico no menu  Register
 Então o sistema navega para a tela Sign Up
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Home
 Quando clico no link do rodapé Test Description - User Stories
 Então o sistema navega para a tela User Stories
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Home
 Quando clico no link do rodapé Track a Bug
 Então o sistema navega para a tela Sign In
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu My Tasks
 Então o sistema navega para a tela Sign In
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Sign In
 Então o sistema navega para a tela Sign In
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando  preencho os campos Email e Password com "<usuario>" e "<senha>" e clico em SignIn
 Então verifico que estou na tela My Tasks
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no link do rodapé Track a Bug
 Então navego para a tela Bugs I have found so far
 E verifico se o link My Tasks está sendo exibido na NavBar

 Quando clico no menu Home
 E clico no botão My Tasks
 Então verifico que estou na tela My Tasks
 E verifico se o link My Tasks está sendo exibido na NavBar

 Exemplos: 
  |usuario                                  |senha        |
  |ironmaximo2@hotmail.com |Teste@123|


@1.2
Cenário: 1.2-Validar apresentação de mensagem que indica a quem pertence a lista de tarefa 
O usuário deve ver uma mensagem na parte superior da tela de tarefas dizendo que a lista pertence ao registro
do utilizador:
Por exemplo: Se o nome do usuário registrado for John, a mensagem exibida deverá ser "Hey
John, this is your todo list for today:"


  Dado que estou logado no sistema ToDo e na tela MyTasks

  Então o sistema ToDo apresenta  a mensagem que indica a quem pertence a lista de tarefas 

@1.3.1
Esquema do Cenário: 1.3.1-Validar a inclusão de resgitros de tarefas utilizando a tecla Enter
O usuário deve ser capaz de inserir uma nova tarefa pressionando Enter
Inclusão de registro de task com 3 caracteres
  
  Quando  preencho o campo Type a new task here and press enter com "<descricao>"
  E pressiono a tecla Enter do teclado
  Então o sistema inclui o novo registro na lista de tarefas cadastradas "<num_caracteres>"
  Exemplos:
  |descricao  |num_caracteres|
  |123           |3                        |

@1.3.2
Esquema do Cenário: 1.3.2-Validar a inclusão de resgitro de tarefas utilizando o botão de adicionar task
O usuário deve ser capaz de inserir uma nova tarefa clicando no botão adicionar
Inclusão de registro de task com 250 caracteres
 
  Quando preencho o campo Type a new task here and press enter com "<descricao>"
  E clico no botão de adicionar task
  Então o sistema inclui o novo registro na lista de tarefas cadastradas "<num_caracteres>"
  Exemplos:
  |descricao                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |num_caracteres|
  |@#$%&&*+asdfghj1234567890123450123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789|250                    |

@1.4
Esquema do Cenário: 1.4-Validar tentativa de inclusão de regitro utilizando dois caracteres.
  Quando preencho o campo Type a new task here and press enter com "<descricao>"
  E pressiono a tecla Enter do teclado
  Então o sistema não inclui o novo registro

  Exemplos:
  |descricao |
  |12            |

@1.5
Esquema do Cenário: 1.5-Validar tentativa de inclusão de registro utilizando  duzentos e cinquenta e um caracteres.
  Quando preencho o campo Type a new task here and press enter com "<descricao>"
  E clico no botão de adicionar task
  Então o sistema não inclui o novo registro      

  Exemplos:
  |descricao                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
  |@#$%&&*+asdfghj12345678901234501234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891|              

@1.6
Esquema do Cenário: 1.6-Validar que quando a tarefa é adicionada, deve ser anexada à lista de tarefas criadas.
 Após Adicionada, a tarefa deve ser anexada à lista de tarefas criadas.
  Quando confiro o número de registros cadastrados até o momento
  E preencho o campo Type a new task here and press enter com "<descricao>"
  E pressiono a tecla Enter do teclado
  Então o sistema inclui o novo registro e soma mais um ao número de registros cadastrados 
   Exemplos:
  |descricao     |
  |0123456789|

@ignore
Cenário: -Validar marcação de tarefa como pronta
  Quando marco uma tarefa como pronta.
  Então o sistema exibe a tarefa marcada como pronta  na cor verde
  
@ignore
Cenário: 09-Validar marcação de tarefa como pública
  Quando marco uma tarefa como pronta.
  Então o sistema exibe a opção pronta marcada

@criarTask
Cenário: Criar uma task simples
 Quando crio uma task simples 







