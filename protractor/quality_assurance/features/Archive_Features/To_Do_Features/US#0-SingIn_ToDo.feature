#language: pt
@avenue
@login
@0.1
@1.2
@1.3.1
@1.3.2
@1.4
@1.5
@1.6
@02



Funcionalidade: Login

Esquema do Cenário: Efetuar Login no sistema ToDo
  Dado que estou na tela Home do sistema ToDo, area não logada
  Quando clico no menu Sign In
  E preencho os campos Email e Password com "<usuario>" e "<senha>" e clico em SignIn
  Então verifico a mensagem de sucesso no login

  Exemplos: 
  |usuario                                  |senha        |
  |ironmaximo2@hotmail.com |Teste@123|

@efetuarLogin
 Cenário: login
 Quando efetuo login
  
  