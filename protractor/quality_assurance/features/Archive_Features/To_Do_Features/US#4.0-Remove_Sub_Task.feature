#language: pt
@avenue
@RemoveSubTask
@4.0


Funcionalidade: Exclusão de Sub Tasks

Contexto: Acessar tela de Subtasks

 Contexto: Passos para Satisfazer Pré-condições e validar  a funcionalidade Subtask
 Quando efetuo login
 E clico no menu My Tasks
 E crio uma task simples
 E clico em Manage Subtasks.
 E incluo um registro de SubTask


Cenário: Excluir Sub Tasks
  Quando clico no botão Remove Subtasks
  Então o sistema exclui registros de Sub Tasks  
