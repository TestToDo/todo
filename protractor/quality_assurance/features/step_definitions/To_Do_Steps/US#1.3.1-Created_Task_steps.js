//Informar bibliotecas que serão utilizadas nos testes
const { When, Then } = require('cucumber');
const chai = require('chai');

chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
//var myTasksPage = require('../../page_objects/Teste_Avenue_po/myTasks_po.js');
var myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
var myTasks = new myTasksPage();



//var n = 0

When('preencho o campo Type a new task here and press enter com {string}', async function(descricao) {
	scenario = this;
	await element.all(by.repeater('task in tasks')).count().then(async function(size) {
	sizex = size
	})
	await scenario.attach('Número de tasks cadastradas no inicio do teste é: ' + sizex);
	await myTasks.preencherCpTask(descricao);
	
});

When('pressiono a tecla Enter do teclado', async function() {
	var input = element(by.model('newTask.body'));
	await input.sendKeys(protractor.Key.ENTER);

})
	


Then('o sistema inclui o novo registro na lista de tarefas cadastradas {string}', async  function (num_caracteres) {
	//VER
	let scenario = this;
	await element.all(by.repeater('task in tasks')).count().then(async function(size2) {
		await scenario.attach('Número de tasks cadastradas após a inclusão do novo registro é: ' + size2);
		chai.expect(size2).to.equal(sizex+=1);
	});

	var myTasks = await element.all(by.css('a.ng-scope.ng-binding.editable.editable-click')).first();
	await chai.expect(myTasks).to.be.displayed;
	await element
		.all(by.css('a.ng-scope.ng-binding.editable.editable-click'))
		.first()
		.getText()
		.then(async function(text) {
			var n = text.length;
			//console.log('O número de caracteres da task cadastrada é: '+n)
			await chai.expect(n).to.equal(eval(num_caracteres));
			await scenario.attach('O número de caracteres da task cadastrada neste teste é: ' + n);
		});
});

When('crio uma task simples', async function () {
	await myTasks.preencherCpTask('TaskForSubTasks');
	var input = element(by.model('newTask.body'));
	await input.sendKeys(protractor.Key.ENTER);

	
  });
