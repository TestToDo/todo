//Informar bibliotecas que serão utilizadas nos testes
const { Given, When, Then } = require('cucumber');
const chai = require('chai');
const SignInPage = require('../../page_objects/To_Do_po/SignIn_ToDo_po');
const signIn = new SignInPage();
const HomePage = require('../../page_objects/To_Do_po/Home_ToDo_po');
const home = new HomePage();
const MenuSuperioToDoPage = require('../../page_objects/To_Do_po/Menu_Superior_ToDo_po');
const menu = new MenuSuperioToDoPage();

Given('que estou na tela Home do sistema ToDo, area não logada', async function() {
	await browser.waitForAngularEnabled(false);
	await browser.get('/');
});

When('clico no menu Sign In', async function() {
	await menu.descatarSignIn();
	await menu.clicarMenuSignIn();
});

When('preencho os campos Email e Password com {string} e {string} e clico em SignIn', async function(usuario, senha) {
	await signIn.preencherEmail(usuario);
	await signIn.descatarCpEmail();

	await signIn.preencherSenha(senha);
	await signIn.descatarCpSenha();

	await signIn.descatarBtSignIn();

	await signIn.clicarBtSignIn();
});

Then('verifico a mensagem de sucesso no login', async function() {
	var msgSucessoText = await home.menSucLogin;

	await home.descatarMenSucLogin();
	await chai.expect(msgSucessoText).to.be.displayed;
	await chai.expect(msgSucessoText).to.contain.text('Signed in successfully.');
});

When('efetuo login', async function () {
	await browser.waitForAngularEnabled(false);

	await browser.get('/');

	await menu.clicarMenuSignIn();

	await signIn.preencherEmail('ironmaximo2@hotmail.com');
	
	await signIn.preencherSenha('Teste@123');

	await signIn.clicarBtSignIn();
  });
