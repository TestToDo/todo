//Informar bibliotecas que serão utilizadas nos testes
const { When, Then } = require('cucumber');
const chai = require('chai');

chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
//var myTasksPage = require('../../page_objects/Teste_Avenue_po/myTasks_po.js');
var myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
var myTasks = new myTasksPage();

When('confiro o número de registros cadastrados até o momento', async function () {     
    scenario = this;
	await element.all(by.repeater('task in tasks')).count().then(async function(size) {
	sizex = size
	})
	await scenario.attach('Número de tasks cadastradas no inicio do teste é: ' + sizex);
    
  });

  Then('o sistema inclui o novo registro e soma mais um ao número de registros cadastrados', async function () {
    let scenario = this;
	await element.all(by.repeater('task in tasks')).count().then(async function(size2) {
		await scenario.attach('Número de tasks cadastradas após a inclusão do novo registro é: ' + size2);
		chai.expect(size2).to.equal(sizex+=1);
	});
         });


