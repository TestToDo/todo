//Informar bibliotecas que serão utilizadas nos testes
const { Given, When, Then } = require('cucumber');
const chai = require('chai');
const EC = protractor.ExpectedConditions;
chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
//var myTasksPage = require('../../page_objects/Teste_Avenue_po/myTasks_po.js');
const myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new myTasksPage();
const MenuSuperioToDoPage = require('../../page_objects/To_Do_po/Menu_Superior_ToDo_po');
const menuSuperior = new MenuSuperioToDoPage();
const HomePage = require('../../page_objects/To_Do_po/Home_ToDo_po');
const home = new HomePage();
const SignUpToDoPage = require('../../page_objects/To_Do_po/SignUp_ToDo_po');
const signUp = new SignUpToDoPage();
const UserStoriesPage = require('../../page_objects/To_Do_po/User_Stories_po');
const userStories = new UserStoriesPage();
const LinksRodapePage = require('../../page_objects/To_Do_po/Links_Rodape_ToDo_po');
const linksRodape = new LinksRodapePage();
const SignInPage = require('../../page_objects/To_Do_po/SignIn_ToDo_po');
const signIn = new SignInPage();
const TrackABugPage = require('../../page_objects/To_Do_po/Truck_a_Bug_ToDo_po');
const TrackABug = new TrackABugPage();

//const Helper = require('../../shared_libs/helper');
//const helper = new Helper();

Then('verifico se o link My Tasks está sendo exibido na NavBar', async function() {
	await chai.expect(menuSuperior.mnMyTasks).to.be.displayed;
	await chai.expect(menuSuperior.mnMyTasks).to.contain.text('My Tasks');
});

When('clico no botão Sign Up da tela Home', async function() {
	await chai.expect(home.btSignUp).to.be.displayed;
	await home.clicarBtSignUp();
});

Then('o sistema navega para a tela Sign Up', async function() {
	browser.wait(EC.presenceOf(signUp.titlePage, 5000));
	await chai.expect(signUp.titlePage).to.be.displayed;
	await chai.expect(signUp.titlePage).to.contain.text('Sign up');
});

When('clico no menu Home', async function() {
	await menuSuperior.clicarMenuHome();
});

When('clico no botão Test Description', async function() {
	await home.clicarBtTestDesc();
});

Then('o sistema navega para a tela User Stories', async function() {
	await chai.expect(userStories.titlePage).to.be.displayed;
	await chai.expect(userStories.titlePage).to.contain.text('User Stories');
});

When('clico no menu  Register', async function() {
	await menuSuperior.clicarMenuRegister();
});

When('clico no link do rodapé Test Description - User Stories', async function() {
	await linksRodape.clicarLinkTestDesc();
});

When('clico  no link do rodapé Track a Bug', async function() {
	await linksRodape.clicarLinkBugs();
});

Then('o sistema navega para a tela Sign In', async function() {
	await chai.expect(signIn.titlePage).to.be.displayed;
	await chai.expect(signIn.titlePage).to.contain.text('Sign in');
});

Then('o sistema exibe a tela Home da area logada com o botão My Tasks', async function() {
	await chai.expect(home.btMyTasks).to.be.displayed;
	await chai.expect(home.btMyTasks).to.contain.text('My Tasks');
});

When('clico no link do rodapé Track a Bug', async function() {
	await linksRodape.clicarLinkBugs();
});

Then('navego para a tela Bugs I have found so far', async function() {
	await TrackABug.titlePage;
});

When('clico no menu My Tasks', async function() {
	await menuSuperior.clicarMenuMyTasks();
});
