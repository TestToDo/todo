//Informar bibliotecas que serão utilizadas nos testes
const { Given, When, Then } = require('cucumber');
const chai = require('chai');
var faker = require('faker');

var nomeSubTask = faker.name.findName();

const MyTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new MyTasksPage();
const   subTasksPage = require('../../page_objects/To_Do_po/SubTasks_ToDo_po');
const subTasks = new subTasksPage()


Given('que estou na tela My Tasks com ao menos um registro de task cadastrado', async function() {
    scenario = this;
    await chai.expect( myTasks.titleGrid).to.be.displayed;
    await chai.expect( myTasks.btManSubTask).to.be.displayed;


    await element(by.css('button.btn.btn-xs.btn-primary.ng-binding')).getText().then(function(text) {
        texto = text

      });
      var NumSubTasksBt = texto.substring(1,2)
      await scenario.attach('Número de subtasks exibida no botão é: ' + NumSubTasksBt);

	
});

When('clico em Manage Subtasks.', async function() {
    await myTasks.clicarBtManSubTask()
});

Then('o sistema apresenta a modal Editing Task <número da task>', async function() {
    //await browser.sleep(2000)
    await browser.wait(function() {
    return element(by.css(".modal-title.ng-binding")).isPresent()});
    await chai.expect(subTasks.titleModalSub).to.contain.text('Editing Task');
    scenario = this;
	await element.all(by.repeater('sub_task in task.sub_tasks')).count().then(async function(size) {
	sizeSub = size
	})
	await scenario.attach('Número de subtasks cadastradas no início do teste é: ' + sizeSub);
});

When('preencho os campos {string} e {string} e clico no botão Add', async function(SubTaskDescription, DueDate) {
    await subTasks.preencherCpSubTaskDesc(SubTaskDescription)
    
    await subTasks.preencherCpDueDate(DueDate)
    
    await subTasks.clicarBtAddSubTask()

});


Then('um novo registro de SubTask será incluído na grid SubTasks of this ToDo.', async function() {
    let scenario = this;
	await element.all(by.repeater('sub_task in task.sub_tasks')).count().then(async function(size2) {
		await scenario.attach('Número de Subtasks cadastradas após a inclusão do novo registro é: ' + size2);
        chai.expect(size2).to.equal(sizeSub+=1);
        
    });
    //$('td.task_body.col-md-8').get(1)
    var  DescLinha1 = $('td.task_body.col-md-8')
    await DescLinha1.getText()
    .then(async function(text) {
        var n = text.length;
        //console.log('O número de caracteres da task cadastrada é: '+n)
        //await chai.expect(n).to.equal(eval(num_caracteres));
        await scenario.attach('O número de caracteres da Subtask cadastrada neste teste é: ' + n);
    });
});

When('incluo um registro de SubTask', async function () {
    await subTasks.preencherCpSubTaskDesc(nomeSubTask)
    
    await subTasks.preencherCpDueDate('10082019')
    
    await subTasks.clicarBtAddSubTask()

    
  });

//2.2

When('clico em close', async function () {
    await subTasks.clicarBtClose()
  
  });

Then('o botão Manage Subtasks apresenta o número de subtasks da tarefa', async function () {
    scenario = this;
    await chai.expect( myTasks.titleGrid).to.be.displayed;
    await chai.expect( myTasks.btManSubTask).to.be.displayed;

    //await scenario.attach('Número de subtasks botão é: ' + SubtasksBt);

    await element(by.css('button.btn.btn-xs.btn-primary.ng-binding')).getText().then(function(text) {
        texto = text
        //expect(text).toEqual('A risk name');  
      });
      var NumSubTasksBt = texto.substring(1,2)
      await scenario.attach('Número de subtasks exibida no botão é: ' + NumSubTasksBt);
      chai.expect(NumSubTasksBt).to.equal('1');

  });

  //@2.3.1

  Then('valido que as informações de id e descrição da tarefa são somente leitura.', async function () {
    scenario = this;

    var nomeTarefa=  await element(by.id('edit_task')).getAttribute('value')
    await subTasks.preencherCpToDo(nomeSubTask)
    var nomeTarefa2=  await element(by.id('edit_task')).getAttribute('value')

    await scenario.attach('A descrição da subtasks  era inicialmente: ' + nomeTarefa +' e agora é: '+ nomeTarefa2 );
    chai.expect(nomeTarefa).to.equal(nomeTarefa2);

  });

  //@2.3.2.2
  Then('o sistema não inclui a nova subtarefa', async function () {
    let scenario = this;
	await element.all(by.repeater('sub_task in task.sub_tasks')).count().then(async function(size2) {
	await scenario.attach('Número de Subtasks cadastradas é: ' + size2 + ' deve permanecer: ' +sizeSub + ' .O sistema deve impedir o cadastro.');
    chai.expect(size2).to.equal(sizeSub);
        
    });
    await browser.executeScript("document.body.style.zoom='0.75'");
  });

  //@2.3.3
  When('preencho os campos {string} e {string} e pressiono a tecla Enter.', async function (SubTaskDescription, DueDate) {
    await subTasks.preencherCpSubTaskDesc(SubTaskDescription)
    
    await subTasks.preencherCpDueDate(DueDate)

    var input = element(by.model('subTask.body'));
    await input.sendKeys(protractor.Key.ENTER);

    var input2 = element(by.model('subTask.due_date'));
    await input2.sendKeys(protractor.Key.ENTER);
    

  });

  //@2.4

  
  When('sem preencher os campos SubTask Description e DueDate  clico em Add', async function () {
    await subTasks.clicarBtAddSubTask()
  });

  

