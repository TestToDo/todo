//Informar bibliotecas que serão utilizadas nos testes
const { Given, When, Then } = require('cucumber');
const chai = require('chai');

const MyTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new MyTasksPage();
const   subTasksPage = require('../../page_objects/To_Do_po/SubTasks_ToDo_po');
const subTasks = new subTasksPage()
When('clico no botão Remove Subtasks', async function() {
    scenario = this;
	await element.all(by.repeater('sub_task in task.sub_tasks')).count().then(async function(size) {
	sizeSub = size
	})
	await scenario.attach('Número de subtasks cadastradas no início dos testes é: ' + sizeSub);

await subTasks.clicarBtRemSubTask()

});

Then('o sistema exclui registros de Sub Tasks', async function() {
    let scenario = this;
	await element.all(by.repeater('sub_task in task.sub_tasks')).count().then(async function(size2) {
		await scenario.attach('Número de Subtasks cadastradas após a exclusão de um registro é: ' + size2);
        chai.expect(size2).to.equal(sizeSub-1);
        
    });

});
