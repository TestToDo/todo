//Informar bibliotecas que serão utilizadas nos testes
const { Given, When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
const myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new myTasksPage();
var size = 0;

Given('que possuo ao menos um um registro de task cadastrado', async function() {
	await element.all(by.repeater('task in tasks')).count().then(function(size) {
		console.log('Número de tasks cadastradas:' + size);
		chai.expect(size).to.not.equal(0);
	});
});

When('clico no botão Remove', async function() {
	//await myTasksPage.clicarbotaoAdic();
	await element.all(by.repeater('task in tasks')).count().then(async function(size) {
		x = 0;
		while (size > 0) {
			await myTasks.clicarBtRemove();
			size--;
			x == size;
		}
	});

	//	if (size > 0)
	//	await myTasksPage.clicarbotaoRemove();
});

Then('o sistema remove o registro de tarefa', async function() {
	var scenario = this;
    

	await element.all(by.repeater('task in tasks')).count().then(async function(size2) {
		console.log('Número de tasks cadastradas:' + size2);
		scenario.attach("'Número de tasks cadastradas no momento: "+ size2)
		chai.expect(size2).to.equal(0);
	});
	//	should.not.exist(myTasksPage.btRemove);
	//await chai.expect(myTasksPage.btRemove).to.not.exist;
});
