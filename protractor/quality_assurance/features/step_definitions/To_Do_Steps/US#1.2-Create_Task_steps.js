const { Given, When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
const myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new myTasksPage();

Given('que estou logado no sistema ToDo e na tela MyTasks', async function() {
	//await element(by.css(".btn.btn-lg.btn-success")).click();

	await chai.expect(myTasks.titleGrid).to.be.displayed;
	await chai.expect(myTasks.titleGrid).to.contain.text('To be Done');
});

////
Then('o sistema ToDo apresenta  a mensagem que indica a quem pertence a lista de tarefas', async function() {
	await element.all(by.repeater('task in tasks')).count().then(function(size) {
		console.log('Número de tasks cadastradas:' + size);
		//chai.expect(size).to.equal(64);
	});

	await chai.expect(myTasks.titlePage).to.contain.text('Hey Marcio Denis Aguiar, this is your todo list for today:');
});
