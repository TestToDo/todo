const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-url'));
chai.use(require('chai-smoothie'));
const HomePage = require('../../page_objects/To_Do_po/Home_ToDo_po');
const home = new HomePage();
const myTasksPage = require('../../page_objects/To_Do_po/My_Tasks_ToDo_po');
const myTasks = new myTasksPage();

When('clico no botão My Tasks', async function() {
	await home.descatarBtMyTasks();
	await home.clicarBtMyTasks();
});

Then('verifico que estou na tela My Tasks', async function() {
	await myTasks.descatarTituloGrid();
	await chai.expect(myTasks.titlePage).to.be.displayed;
	await chai.expect(myTasks.titlePage).to.contain.text('ToDo List');
	await chai.expect(myTasks.titleGrid).to.be.displayed;
	await chai.expect(myTasks.titleGrid).to.contain.text('To be Done');
});
