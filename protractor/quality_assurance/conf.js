const jsonReports = process.cwd() + "C:/Relatórios/json";
const Reporter = require("/Source - Automação - AC/Quality_Assurance/protractor/quality_assurance/support/reporter");

'use strict'
const Data = require('./environments_parameters.json');

const TEST_ENV = process.env.TEST_ENV || 'local'
let environmentParameters

switch (TEST_ENV) {
  case 'local':
    environmentParameters = Data[0].local
    break
}

exports.config = {
  seleniumAddress: environmentParameters.seleniumAddress,
  ignoreUncaughtExceptions: true,
  //directConnect: true,
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  restartBrowserBetweenTests: false,
  getPageTimeout: 700000,
  allScriptsTimeout: 500000,
  rootElement: 'body',
  baseUrl: environmentParameters.baseUrl,
  params: {

  },
  capabilities: {
    browserName: process.env.TEST_BROWSER_NAME || "chrome",

    chromeOptions: {
      args: [
        //"headless",
        "--disable-gpu",
        "--window-size=1280,1024",
        "--Buffer.allocUnsafe()"
      ]
    }
  },
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  specs: [
  
    'features/Archive_Features/To_Do_Features/*.feature',
    'features/Archive_Features/To_Do_Features/US#1/*.feature',
    'features/Archive_Features/To_Do_Features/US#2/*.feature',

  ],
  resultJsonOutputFile: "C:/Relatórios/json/protractor_report.json",
  onPrepare: function () {
    browser.ignoreSynchronization = true;
    browser.manage().window().maximize();
    //require('babel-register');
  },

  cucumberOpts: {
    strict: true,
    format: 'json:results.json',
    
    require: '../features/step_definitions/To_Do_Steps/*.js',
    require: '../features/step_definitions/To_Do_Steps/US#1/*.js',
    require: '../features/step_definitions/To_Do_Steps/US#2/*.js',
    tags: ['@avenue, ~@in-progress'], // @DatabaseTest scenario can be included when the username & password of DB have been configured in Support/database.js
  },
  onComplete: function () {
    Reporter.createHTMLReport();
  }
};